class Employee {
    constructor(name, age, salary) {
        this.name = name;
        this.age = age;
        this.salary = salary;
    }

    get name() {
        return this._name;
    }

    set name(editName) {
        this._name = editName;
    }

    get age() {
        return this._age;
    }

    set age(editAge) {
        this._age = editAge;
    }

    get salary() {
        return this._salary;
    }

    set salary(editSalary) {
        this._salary = editSalary;
    }
}

class Programmer extends Employee {
    constructor(name, age, salary, lang) {
        super(name, age, salary);
        this.lang = lang;
    }

    get lang() {
        return this._lang;
    }

    set lang(editLang) {
        this._lang = editLang;
    }

    get salary() {
        return this._salary * 3;
    }

    set salary(editSalary) {
        this._salary = editSalary;
    }
}

const developer = new Programmer("Oleksander", 21, 45000, "JavaScript");
const developer2 = new Programmer("Vadim", 29, 75000, ["Java", "Python"]);
const developer3 = new Programmer("Vanya", 45, 25000, ["C++", "React"]);

console.log("============= developer #1")
console.log("Name:", developer.name);
console.log("Age:", developer.age);
console.log("Salary:", developer.salary);
console.log("Languages programming:", developer.lang);

console.log("============= developer #2")
console.log("Name:", developer2.name);
console.log("Age:", developer2.age);
console.log("Salary:", developer2.salary);
console.log("Languages programming:", developer2.lang);

console.log("============= developer #3")
console.log("Name:", developer3.name);
console.log("Age:", developer3.age);
console.log("Salary:", developer3.salary);
console.log("Languages programming:", developer3.lang);



